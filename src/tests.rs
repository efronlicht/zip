use super::Zip;

#[test]
fn fill_with() {
    let q = vec![0_usize; 4];
    let r = vec![1_usize; 7];
    let want = vec![(0_usize, 1_usize); 7];
    let got: Vec<(usize, usize)> = q.into_iter().zip_fill_with(r, 0).collect();
    assert_eq!(want, got);

    let want = vec![
        ("abc".to_string(), "foo".to_string()),
        ("abc".to_string(), "bar".to_string()),
    ];
    let got: Vec<(String, String)> = Vec::new()
        .into_iter()
        .zip_fill_with(
            vec!["foo".to_string(), "bar".to_string()],
            "abc".to_string(),
        )
        .collect();
    assert_eq!(got, want)
}
#[test]
fn fill() {
    let mut i: u8 = 0;
    let filler = || {
        let c = (b'a' + i) as char;
        i += 1;
        c
    };
    let want = vec![('A', 'a'), ('B', 'b'), ('C', 'c'), ('D', 'd')];
    let q = vec!['A', 'B', 'C', 'D'];
    let got: Vec<(char, char)> = q.clone().zip_fill(Vec::new(), filler).collect();
    assert_eq!(want, got);

    let want_rev = vec![('e', 'A'), ('f', 'B'), ('g', 'C'), ('h', 'D')];
    let mut i = 4;
    let filler = || {
        let c = (b'a' + i) as char;
        i += 1;
        c
    };
    let got_reversed: Vec<(char, char)> = Vec::new().zip_fill(q, filler).collect();
    assert_eq!(got_reversed, want_rev)
}
#[test]
fn fill_default() {
    let a: Vec<usize> = vec![1, 3, 5];
    let b: Vec<usize> = vec![2, 4, 6, 8];
    let want = vec![(1, 2), (3, 4), (5, 6), (0, 8)];
    let got: Vec<(usize, usize)> = a.zip_fill_default(b).collect();
    assert_eq!(want, got)
}
#[test]
fn size_hint() {
    use std::iter::repeat;
    assert_eq!(
        (20, Some(20)),
        repeat(0_usize)
            .take(10)
            .zip_fill_default(repeat(0).take(20))
            .size_hint()
    );

    let ten = std::iter::repeat(10).take(10);
    let it = ten.zip_fill_default(repeat(0));
    assert_eq!(it.size_hint(), (::std::usize::MAX, None));
}
